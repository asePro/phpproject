<?php

date_default_timezone_set('Europe/Paris');
try{
  // le fichier de BD s'appellera contacts.sqlite3
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  $file_db->exec("CREATE TABLE IF NOT EXISTS FILM (
                    idFilm INTEGER PRIMARY KEY,
                    titre TEXT,
                    idReal INTEGER,
                    description TEXT,
                    annee DATE,
                    nationalite TEXT,
                    illustration TEXT,
                    FOREIGN KEY(idReal) REFERENCES REALI(idReal) ON DELETE CASCADE);
                  CREATE TABLE IF NOT EXISTS REALI (
                    idReal INTEGER PRIMARY KEY,
                    nom TEXT,
                    Prenom TEXT,
                    ageReal DATE,
                    nationaliteReal TEXT,
                    photo TEXT);
                  CREATE TABLE IF NOT EXISTS GENRE (
                    idGenre INTEGER PRIMARY KEY,
                    genre TEXT);
                  CREATE TABLE IF NOT EXISTS APPARTIENT(
                    idGenre INTEGER,
                    idFilm INTEGER,
                    FOREIGN KEY(idFilm) REFERENCES FILM(idFilm),
                    FOREIGN KEY(idGenre) REFERENCES GENRE(idGenre));");

  #$instances=array();

  #$insert="INSERT INTO contacts (nom, prenom, time) VALUES (:nom, :prenom , :time)";
  #$stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  #$stmt->bindParam(':nom',$nom);
  #$stmt->bindParam(':prenom',$prenom);
  #$stmt->bindParam(':time',$time);

  #foreach ($contacts as $c){
  #  $nom=$c['nom'];
  #  $prenom=$c['prenom'];
  #  $time=$c['time'];
  #  $stmt->execute();
  #}

  //echo "Insertion en base reussie !";
  // on va tester le contenu de la table contacts
  //$result=$file_db->query('SELECT * from contacts');
//  foreach ($result as $m){
    //echo "<br/>\n".$m['prenom'].' '.$m['nom'].' '.date('Y-m-d H:i:s',$m['time']);
//  }
  // on ferme la connexion
  $file_db=null;
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
?>
