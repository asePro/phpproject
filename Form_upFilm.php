<?php

date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}

include 'header.php';

if (!isset($_POST['idfilm']) && !isset($_POST['titre'],$_POST['real'],$_POST['desc'],$_POST['annee'],$_POST['pays'],$_POST['img'])){
?>
<div>
  <h2>Modifier un film</h2>
</div>
<form action="Form_upFilm.php" method="POST">
  <div class="form-group input-group mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Parcourir les réalisateurs</label>
    </div>
  <select class="custom-select" id="inputGroupSelect01" name="idfilm">
  <option selected>Choisir un film</option>
  <?php
  $stmt = $file_db->query("SELECT * FROM FILM ");
  foreach($stmt as $film){
        echo "<option value='".$film["idFilm"]."'>";
        echo $film["titre"];
        echo "</option>";
    }
    ?>
  </select>
  <input type="submit" class="btn btn-primary" name="ok" value="Valider">
  </div>
</form>
<?php
}
else{
  if (!isset($_POST['titre'],$_POST['real'],$_POST['desc'],$_POST['annee'],$_POST['pays'],$_POST['img'])){
  $idfilm = $_POST['idfilm'];
  $film = $file_db->query("SELECT * FROM FILM NATURAL JOIN REALI WHERE idFilm = ".$_POST['idfilm']."");
    foreach ($film as $f) {
  ?>
  <div id ="form" class="w-50 p-3 mx-auto formphp">
    <div>
      <h2>Modifier un film</h2>
    </div>
    <form action="Form_upFilm.php" method="POST">
      <input type="hidden" name="idfilm" value="<?php echo $idfilm; ?>">
      <div class="form-group">
        <label>Titre du film :</label>
        <input type="text" class="form-control" name="titre" value="<?php echo $f['titre'];?>" required>
      </div>
        <div class="form-group input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Parcourir les réalisateurs</label>
        </div>
        <select class="custom-select" id="inputGroupSelect01" name="real" required>
            <option value='<?php echo $f['idReal']; ?>' selected><?php echo $f["Prenom"]." ".$f["nom"] ?></option>
            <?php
            $stmt = $file_db->query("SELECT * FROM REALI ");
            foreach($stmt as $real){

                echo "<option value='".$real["idReal"]."'>";
                echo $real["Prenom"]." ".$real["nom"];
                echo "</option>";
            }
            ?>
        </select>
      </div>

      <div class="form-group">
        <label>Description :</label>
        <input type="textarea" class="form-control"  name="desc" value="<?php echo $f['description']; ?>" required>
      </div>

      <div class="form-group">
        <label>Année :</label>
        <input type="text" class="form-control" name="annee" value="<?php echo $f['annee']; ?>" required>
      </div>

      <div class="form-group">
        <label>Nationalité :</label>
        <input type="text" class="form-control" name="pays" value="<?php echo $f['nationalite']; ?>" required>
      </div>

      <div class="form-group">
        <label>URL photo :</label>
        <input type="text" class="form-control" name="img" value="<?php echo $f['illustration']; ?>">
      </div>

      <input type="submit" class="btn btn-primary" name="ok" value="Valider">
    </form>
  </div>
<?php
}
}
else{
  modif_film($_POST['idfilm'],$_POST['titre'],$_POST['real'],$_POST['desc'],$_POST['annee'],$_POST['pays'],$_POST['img']);
}
}
include 'footer.php'; ?>
