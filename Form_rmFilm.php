<?php
date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
if (!isset($_POST['idfilm'])){
?>
<div>
  <h2>Supprimer un film</h2>
</div>
<form action="Form_rmFilm.php" method="POST">
  <div class="form-group input-group mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Parcourir les réalisateurs</label>
    </div>
  <select class="custom-select" id="inputGroupSelect01" name="idfilm">
  <option selected>Choisir un film</option>
  <?php
  $stmt = $file_db->query("SELECT * FROM FILM ");
  foreach($stmt as $film){
        echo "<option value='".$film["idFilm"]."'>";
        echo $film["titre"];
        echo "</option>";
    }
    ?>
  </select>
  <input type="submit" class="btn btn-primary" name="ok" value="Valider">
  </div>
</form>
<?php
}
else{
  rmFilm($_POST['idfilm']);
}
include 'footer.php' ?>
