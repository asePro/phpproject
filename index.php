<?php date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
?>
    <div class="container">
      <h2>Films à l'affiche</h2>
      <div class="row">
      <?php
      $stmt = $file_db->query("SELECT * FROM FILM NATURAL JOIN REALI ");

      foreach($stmt as $film){
        $stmtGenre = $file_db->query("SELECT * FROM GENRE NATURAL JOIN APPARTIENT NATURAL JOIN FILM WHERE idFilm = ".$film['idFilm']);
        $genres = array();
        foreach ($stmtGenre as $g) {
          array_push($genres,$g['genre']);
        }
        echo "
        <div class='col-6'>
                <div class='card flex-md-row mb-2 box-shadow h-md-250 cardfilm'>
                  <div class='card-body d-flex flex-column align-items-start'>
                    <h3 class='mb-0'>".$film["titre"]."</h3>
                    <div class='mb-1 text-muted'>Réalisateur : ".$film["nom"]." ".$film["Prenom"]."</div>
                    <div class='mb-1 text-muted'>Année : ".$film["annee"]."</div>
                    <div class='mb-1 text-muted'>Genre : ".implode( ", ", $genres )."</div>
                    <p class='card-text mb-auto'>Description : ".$film["description"]."</p>
                  </div>
                  <img class='image_film card-img-right flex-auto d-none d-md-block' src='".$film['illustration']."' alt='".$film["titre"]."' height='250' width='auto'>
                </div>
              </div>
        ";
      }

  ?>
  </div>
</div>
        <?php
        $request = $file_db->query("SELECT DISTINCT idGenre, genre FROM GENRE NATURAL JOIN APPARTIENT ");
        foreach ($request as $g) {
          echo "<div class='container'>";
          echo "<h2>".$g['genre']."</h2>";
          echo "<div class='row'>";
          affichebyGenre($g['idGenre']);
          echo "</div>";
          echo "</div>";
          echo "</div>";
        }
include 'footer.php';?>
