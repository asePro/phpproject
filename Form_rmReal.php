<?php
date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
if (!isset($_POST['real'])){
?>
<div>
  <h2>Supprimer un Réalisateur</h2>
</div>
<form action="Form_rmReal.php" method="POST">
    <div class="form-group input-group mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="inputGroupSelect01">Parcourir réalisateur</label>
    </div>
    <select class="custom-select" id="inputGroupSelect01" name="real">
        <option selected>Choisir un réalisateur</option>
        <?php
        $stmt = $file_db->query("SELECT * FROM REALI ");
        foreach($stmt as $real){

            echo "<option value='".$real["idReal"]."'>";
            echo $real["Prenom"]." ".$real["nom"];
            echo "</option>";
        }
        ?>
    </select>
  <input type="submit" class="btn btn-primary" name="ok" value="Valider">
  </div>
</form>
<?php
}
else{
  rmReal($_POST['real']);
}
include 'footer.php' ?>
