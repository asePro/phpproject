<?php

date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}

include 'header.php';
if (!isset($_POST['nom'])){
?>
    <div id ="form" class="w-50 p-3 mx-auto formphp">
      <div>
        <h2>Ajouter un genre</h2>
      </div>
      <form action="Form_genre.php" method="POST">
        <div class="form-group">
          <label>Nom du genre:</label>
          <input type="text" class="form-control" name="nom" value="" required>
        </div>

        <input type="submit" class="btn btn-primary" name="ok" value="Valider">
      </form>
    </div>
<?php
}
else{
  addGenre($_POST['nom']);
} ?>
