<?php
date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
if (isset($_POST["recherche"])){
  $txt = $_POST["recherche"];
  $insert="SELECT * FROM FILM NATURAL JOIN REALI WHERE titre like '%$txt%' or nom like '%$txt%' or Prenom like '%$txt%'";
  $stmt=$file_db->query($insert);
  echo "<h2>Resultat de votre recherche : ".$_POST["recherche"]."</h2>";
  foreach ($stmt as $film) {
    echo "
    <div class='col-md-6'>
            <div class='card flex-md-row mb-2 box-shadow h-md-250'>
              <div class='card-body d-flex flex-column align-items-start'>
                <h3 class='mb-0'>".$film["titre"]."</h3>
                <div class='mb-1 text-muted'>Réalisateur : ".$film["nom"]." ".$film["Prenom"]."</div>
                <p class='card-text mb-auto'>Description : ".$film["description"]."</p>
              </div>
              <img class='card-img-right flex-auto d-none d-md-block' src='".$film['illustration']."' alt='".$film["titre"]."' height='250' width='auto'>
            </div>
          </div>
      </div>
    ";
  }
}
include 'footer.php'?>
