

<?php

date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}

include 'header.php';

if (!isset($_POST['titre'],$_POST['real'],$_POST['desc'],$_POST['annee'],$_POST['pays'],$_POST['img'])){?>


    <div id ="form" class="w-50 p-3 mx-auto formphp">
      <div>
        <h2>Ajouter un film</h2>
      </div>
      <form action="Form_film.php" method="POST">
        <div class="form-group">
          <label>Titre du film :</label>
          <input type="text" class="form-control" name="titre" value="" required>
        </div>
          <div class="form-group input-group mb-3">
          <div class="input-group-prepend">
              <label class="input-group-text" for="inputGroupSelect01">Parcourir les réalisateurs</label>
          </div>
          <select class="custom-select" id="inputGroupSelect01" name="real" required>
              <option selected>Choisir réalisateur</option>
              <?php
              $stmt = $file_db->query("SELECT * FROM REALI ");
              foreach($stmt as $real){

                  echo "<option value='".$real["idReal"]."'>";
                  echo $real["Prenom"]." ".$real["nom"];
                  echo "</option>";
              }
              ?>
          </select>
        </div>
        <div class="form-group input-group mb-3">
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Ajouter un genre</label>
        </div>

        <?php
        $stmt = $file_db->query("SELECT * FROM GENRE ");
        foreach($stmt as $genre){
            echo "<div class='form-check form-check-inline p-2'><input class='form-check-input' type='checkbox' name='genre[]' value=".$genre['idGenre'].">";
            echo $genre["genre"]."</div>";
        }
         ?>
      </div>

        <div class="form-group">
          <label>Description :</label>
          <input type="textarea" class="form-control"  name="desc" value="" required>
        </div>

        <div class="form-group">
          <label>Année :</label>
          <input type="text" class="form-control" name="annee" value="" required>
        </div>

        <div class="form-group">
          <label>Nationalité :</label>
          <input type="text" class="form-control" name="pays" value="" required>
        </div>

        <div class="form-group">
          <label>URL photo :</label>
          <input type="text" class="form-control" name="img" value="">
        </div>

        <input type="submit" class="btn btn-primary" name="ok" value="Valider">
      </form>
    </div>
<?php
}
else{
  addFilm($_POST['titre'],$_POST['real'],$_POST['desc'],$_POST['annee'],$_POST['pays'],$_POST['img']);
  $idFilm = $file_db->query("SELECT max(idFilm) FROM FILM ");
  $idFilm = $idFilm->fetch();
  foreach ($_POST['genre'] as $g) {
    attribueGenre($idFilm[0],$g);
  }




}
include 'footer.php'; ?>
