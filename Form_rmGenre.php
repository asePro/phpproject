<?php
date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
if (!isset($_POST['idGenre'])){
?>
<div>
  <h2>Supprimer un genre</h2>
</div>
<form action="Form_rmGenre.php" method="POST">
  <div class="form-group input-group mb-3">
    <div class="input-group-prepend">
      <label class="input-group-text" for="inputGroupSelect01">Parcourir les Genres</label>
    </div>
  <select class="custom-select" id="inputGroupSelect01" name="idGenre">
  <option selected>Choisir un Genre</option>
  <?php
  $stmt = $file_db->query("SELECT * FROM GENRE ");
  foreach($stmt as $genre){
        echo "<option value='".$genre["idGenre"]."'>";
        echo $genre["genre"];
        echo "</option>";
    }
    ?>
  </select>
  <input type="submit" class="btn btn-primary" name="ok" value="Valider">
  </div>
</form>
<?php
}
else{
  rmGenre($_POST['idGenre']);
}
include 'footer.php' ?>
