<?php

date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}

include 'header.php';
if (!isset($_POST['nom'],$_POST['prenom'],$_POST['age'],$_POST['pays'],$_POST['photo'])){
?>
    <div id ="form" class="w-50 p-3 mx-auto formphp">
      <div>
        <h2>Ajouter un réalisateur</h2>
      </div>
      <form action="Form_real.php" method="POST">
        <div class="form-group">
          <label>Nom :</label>
          <input type="text" class="form-control" name="nom" value="" required>
        </div>
        <div class="form-group">
          <label>Prénom :</label>
          <input type="textarea" class="form-control"  name="prenom" value="" required>
        </div>

        <div class="form-group">
          <label>Age :</label>
          <input type="text" class="form-control" name="age" value="" required>
        </div>

        <div class="form-group">
          <label>Nationalité :</label>
          <input type="text" class="form-control" name="pays" value="" required>
        </div>

        <div class="form-group">
          <label>URL photo :</label>
          <input type="text" class="form-control" name="photo" value="">
        </div>

        <input type="submit" class="btn btn-primary" name="ok" value="Valider">
      </form>
    </div>
<?php
}
else{
  addReal($_POST['nom'],$_POST['prenom'],$_POST['age'],$_POST['pays'],$_POST['photo']);
}
include 'footer.php';

// http://garridodiaz.com/check-if-url-exists-and-is-online-php/
// @$headers = get_headers($url);
// if (preg_match('/^HTTP/d.ds+(200|301|302)/', $headers[0])){
//    return true;
// }
// else return false;
?>
