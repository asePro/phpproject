<link rel="stylesheet" href="css/master.css">
<?php

function addFilm($titre,$idreal,$desc,$ans,$nat,$url)
{
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $insert="INSERT INTO FILM (titre,idReal,description,annee,nationalite,illustration) VALUES (:titre, :idReal, :description, :annee, :nationalite, :url)";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':titre',$titre);
  $stmt->bindParam(':idReal',$idreal);
  $stmt->bindParam(':description',$desc);
  $stmt->bindParam(':annee',$ans);
  $stmt->bindParam(':nationalite',$nat);
  $stmt->bindParam(':url',$url);
  $stmt->execute();
  echo "<p class='Ok'>Le film à bien été ajouté !</p>";
}

function rmFilm($idFilm){
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $file_db->query("DELETE FROM FILM WHERE idFilm = $idFilm");
  $file_db->query("DELETE FROM APPARTIENT WHERE idFilm = $idFilm");
  echo "<p class='Ok'>Film supprimé</p>";
}
function addReal($nom,$prenom,$age,$nat,$url)
{
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $insert="INSERT INTO REALI(nom,prenom,ageReal,nationaliteReal,photo) VALUES (:nom, :prenom, :ageReal, :nationaliteReal, :url)";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':nom',$nom);
  $stmt->bindParam(':prenom',$prenom);
  $stmt->bindParam(':ageReal',$age);
  $stmt->bindParam(':nationaliteReal',$nat);
  $stmt->bindParam(':url',$url);
  $stmt->execute();
  echo "<p class='Ok'>Realisateur ajouté</p>";
}
function rmReal($idReal){
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $file_db->query("DELETE FROM REALI WHERE idReal = $idReal");
  echo "<p class='Ok'>Realisateur supprimé</p>";
}
function addGenre($nomgenre)
{
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $insert="INSERT INTO GENRE(genre) VALUES (:nom)";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':nom',$nomgenre);
  $stmt->execute();
  echo "<p class='Ok'>Genre ajouté</p>";
}
function rmGenre($idGenre){
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $delete = $file_db->prepare("DELETE FROM GENRE WHERE idGenre = $idGenre");
  $delete->execute();
  echo "<p class='Ok'>Genre supprimé</p>";
}
function attribueGenre($idFilm,$idGenre){
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $insert="INSERT INTO APPARTIENT(IdGenre,Idfilm) VALUES (:Idgenre,:idFilm)";
  $stmt=$file_db->prepare($insert);
  // on lie les parametres aux variables
  $stmt->bindParam(':idFilm',$idFilm);
  $stmt->bindParam(':Idgenre',$idGenre);
  $stmt->execute();
}
function modif_film($idFilm,$titre,$idreal,$desc,$ans,$nat,$url)
{
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $insert="UPDATE FILM SET titre ='$titre', idReal ='$idreal', description ='$desc', annee = '$ans', nationalite='$nat', illustration='$url' WHERE idFilm = $idFilm ";
  $stmt=$file_db->query($insert);
  echo "<p class='Ok'>Le film a été modifié !</p>";
}
function natio($nat){
    echo "<img src='img/".$nat.".png' alt='$nat' width='50' height='auto'>";
}
function affichebyGenre($idGenre){
  try{
    $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
    $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
  }
  catch(PDOException $ex){
    echo $ex->getMessage();
  }
  $stmt=$file_db->query("SELECT DISTINCT * FROM FILM NATURAL JOIN REALI NATURAL JOIN APPARTIENT NATURAL JOIN GENRE where idGenre = '$idGenre'");
  foreach ($stmt as $film) {
    echo "
    <div class='col-6'>
            <div class='card flex-md-row mb-2 box-shadow h-md-250 cardfilm'>
              <div class='card-body d-flex flex-column align-items-start'>
                <h3 class='mb-0'>".$film["titre"]."</h3>
                <div class='mb-1 text-muted'>Réalisateur : ".$film["nom"]." ".$film["Prenom"]."</div>
                <div class='mb-1 text-muted'>Année : ".$film["annee"]."</div>
                <p class='card-text mb-auto'>Description : ".$film["description"]."</p>
              </div>
              <img class='image_film card-img-right flex-auto d-none d-md-block' src='".$film['illustration']."' alt='".$film["titre"]."' height='250' width='auto'>
            </div>
          </div>
    ";
  }
}
?>
