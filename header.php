<?php
require_once 'function.php' ?>
<!DOCTYPE html>
<html lang="fr" >
  <head>
    <meta charset="utf-8">
    <title>Film a Gogo</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="css/master.css">
  </head>
  <body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <h3 class="navbar-brand">Film a Gogo</h1>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="liste_real.php">Consulter realisateur</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Action
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="Form_film.php">Ajouter un film</a>
          <a class="dropdown-item" href="Form_upFilm.php">Modifier un film</a>
          <a class="dropdown-item" href="Form_rmFilm.php">Supprimer un film</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="Form_real.php">Ajouter un realisateur</a>
          <a class="dropdown-item" href="Form_rmReal.php">Supprimer un realisateur</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="Form_genre.php">Ajouter un genre</a>
          <a class="dropdown-item" href="Form_rmGenre.php">Supprimer un genre</a>
        </div>
      </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li class="nav-item">
          <form  action="resultat.php" method="post">
            <input type="search" name="recherche"  placeholder="Rechercher">
            <input type="submit" name="Valider" value="Rechercher">
          </form>
      </li>
         </ul>

  </div>
</nav>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
