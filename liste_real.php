<?php date_default_timezone_set('Europe/Paris');
try{
  $file_db=new PDO('sqlite:tmp/CollectFilm.sqlite3');
  $file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

}
catch(PDOException $ex){
  echo $ex->getMessage();
}
include 'header.php';
?>
    <h2>Liste des réalisateurs</h2>

    <div class="container">
      <div class="row">
      <?php
      $stmt = $file_db->query("SELECT * FROM REALI ");
      foreach($stmt as $film){
        echo "<div class='col-xs-6 col-md-3' style='background-color:lightgray;padding:20px;margin:20px;'><a href='#' class='thumbnail'>";
        echo "<h3>".$film["nom"]." ".$film["Prenom"]."</h3>";
        echo "<p>".$film["ageReal"]." ans ".natio($film['nationaliteReal'])."</p>";
        echo "<img src='".$film['photo']."' alt='image' width='100' height='auto'>";
        echo "</a></div>";
      }?>
      </div>
    </div>
    </div>

<?php include 'footer.php';?>
